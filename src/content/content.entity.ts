import { PostEntity } from 'src/post/post.entity';
import { Column, ManyToOne } from 'typeorm';
import { PrimaryGeneratedColumn } from 'typeorm';
import { Entity } from 'typeorm';

@Entity('Content')
export class ContentEntity {
  @PrimaryGeneratedColumn('increment')
  id: string;

  @Column()
  post_id: string;

  @Column()
  url: string;

  @ManyToOne(() => PostEntity, (post) => post.attachments)
  post: PostEntity;
}
