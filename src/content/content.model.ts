import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class ContentModel {
  @Field(() => String)
  url: string;
}
