import { PostEntity } from 'src/post/post.entity';
import { ManyToMany } from 'typeorm';
import { PrimaryColumn } from 'typeorm';
import { Entity } from 'typeorm';

@Entity('Tag')
export class TagEntity {
  @PrimaryColumn('text')
  name: string;

  @ManyToMany(() => PostEntity, (post) => post.tags)
  post: PostEntity;
}
