import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class TagModel {
  @Field(() => String)
  name: string;
}
