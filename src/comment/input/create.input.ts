import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty, IsString } from 'class-validator';

@InputType()
export class CreateCommentInput {
  @IsString()
  @IsNotEmpty()
  @Field(() => String)
  post_id: string;

  @IsString()
  @IsNotEmpty()
  @Field(() => String)
  content: string;
}
