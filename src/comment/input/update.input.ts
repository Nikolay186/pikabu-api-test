import { InputType } from '@nestjs/graphql';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

@InputType()
export class UpdateCommentInput {
  @IsString()
  @IsNotEmpty()
  id: string;

  @IsOptional()
  @IsString()
  content?: string;
}
