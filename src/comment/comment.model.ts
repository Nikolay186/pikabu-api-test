import { Field, Int, ObjectType } from '@nestjs/graphql';
import { PostModel } from '../post/post.model';
import { UserModel } from '../user/model/user.model';
import { UserReactionModel } from '../reactions/models/user-reaction.model';

@ObjectType()
export class CommentModel {
  @Field(() => String)
  id: string;

  @Field(() => String)
  content: string;

  @Field(() => Int)
  rating?: number;

  @Field(() => Date)
  created_date: Date;

  @Field(() => Date)
  updated_date: Date;

  @Field(() => PostModel)
  post: PostModel;

  @Field(() => String)
  post_id: string;

  @Field(() => UserModel)
  author: UserModel;

  @Field(() => String)
  author_id: string;

  @Field(() => [UserReactionModel])
  user_reactions?: UserReactionModel[];
}
