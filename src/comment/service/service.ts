import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { CommentEntity } from '../comment.entity';
import { CreateCommentInput } from '../input/create.input';
import { UpdateCommentInput } from '../input/update.input';

@Injectable()
export class CommentService {
  constructor(private readonly comment_repo: Repository<CommentEntity>) {}

  async create(author_id: string, create_comment_input: CreateCommentInput) {
    return await this.comment_repo.save({
      ...create_comment_input,
      author_id: author_id,
    });
  }

  async find_one(comment_id: string) {
    return await this.comment_repo.findOne(comment_id);
  }

  async update(
    user_id: string,
    comment_id: string,
    upd_comment_input: UpdateCommentInput,
  ) {
    const comment = await this.comment_repo.findOne({
      id: comment_id,
    });
    if (user_id == comment.author_id) {
      return this.comment_repo.save({ ...comment, ...upd_comment_input });
    }
  }

  async remove(user_id: string, comment_id: string) {
    const comment = await this.comment_repo.findOne({
      id: comment_id,
    });
    if (comment.author_id == user_id) {
      return this.comment_repo.remove(comment);
    }
  }

  async find_all(page: number, size: number) {
    return {
      comments_count: (await this.comment_repo.find()).length,
      comments: await this.comment_repo.find({ take: size, skip: page * size }),
    };
  }
}
