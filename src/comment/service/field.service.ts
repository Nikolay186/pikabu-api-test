import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { CommentEntity } from '../comment.entity';

@Injectable()
export class CommentFieldService {
  constructor(
    private readonly comment_repository: Repository<CommentEntity>,
    private readonly comment_reaction_repository: Repository<CommentReactionEntity>,
  ) {}

  async userReactions(comment_id: string) {
    return await this.comment_reaction_repository.find({
      comment_id: comment_id,
    });
  }

  async rating(comment_id: string) {
    return await this.comment_repository.get_rating(comment_id);
  }
}
