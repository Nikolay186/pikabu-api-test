import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { PostEntity } from '../post/post.entity';
import { UserEntity } from '../user/user.entity';

@Entity('Comment')
export class CommentEntity {
  @PrimaryGeneratedColumn('increment')
  id: string;

  @Column()
  post_id: string;

  @Column()
  author_id: string;

  @CreateDateColumn()
  created_date: Date;

  @UpdateDateColumn()
  updated_date: Date;

  @Column()
  content: string;

  @ManyToOne(() => PostEntity, (post) => post.comments)
  post: PostEntity;

  @ManyToOne(() => UserEntity, (user) => user.comments)
  author: UserEntity;

  @OneToMany(() => CommentReactionEntity, (reaction) => reaction.comment)
  user_reactions?: CommentReactionEntity[];

  @OneToMany(
    () => BookmarkedCommentEntity,
    (bookmarkedComment) => bookmarkedComment.comment,
  )
  bookmarks?: BookmarkedCommentEntity[];
}
