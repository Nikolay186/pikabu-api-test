import { Int, Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { CommentModel } from '../comment.model';
import { CommentFieldService } from '../service/field.service';
import { UserModel } from '../../user/model/user.model';
import { PostModel } from '../../post/post.model';
import { UserReactionModel } from '../../reaction/model/user.reaction.model';

@Resolver(() => CommentModel)
export class CommentFieldResolver {
  constructor(private readonly field_service: CommentFieldService) {}

  @ResolveField('post', () => PostModel)
  async find_post(@Parent() comment: CommentModel) {
    return this.field_service.find_post(comment.post_id);
  }

  @ResolveField('author', () => UserModel)
  async find_author(@Parent() comment: CommentModel) {
    return this.field_service.find_author(comment.author_id);
  }

  @ResolveField('user_reactions', () => [UserReactionModel])
  async find_reactions(@Parent() comment: CommentModel) {
    return this.field_service.find_reactions(comment.id);
  }

  @ResolveField('rating', () => Int)
  async get_rating(@Parent() comment: CommentModel): Promise<number> {
    return this.field_service.get_rating(comment.id);
  }
}
