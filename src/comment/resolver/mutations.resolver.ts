import { UseGuards } from '@nestjs/common';
import { Resolver, Mutation, Args } from '@nestjs/graphql';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { Self } from 'src/user/self.decorator';
import { UserDTO } from 'src/user/user.entity';
import { CommentModel } from '../comment.model';
import { CreateCommentInput } from '../input/create.input';
import { UpdateCommentInput } from '../input/update.input';

@UseGuards(JwtAuthGuard)
@Resolver(() => CommentModel)
export class CommentMutationResolver {
  constructor(private readonly comments_service: CommentService) {}

  @Mutation(() => CommentModel)
  create_comment(
    @Self() user: UserDTO,
    @Args('data') data: CreateCommentInput,
  ) {
    return this.comments_service.create(user.id, data);
  }

  @Mutation(() => CommentModel)
  update_comment(
    @Self() user: UserDTO,
    @Args('data') data: UpdateCommentInput,
  ) {
    return this.comments_service.update(user.id, data.id, data);
  }

  @Mutation(() => CommentModel)
  delete_comment(@Self() user: UserDTO, @Args('id') comment_id: string) {
    return this.comments_service.remove(user.id, comment_id);
  }
}
