import { UseGuards, Query } from '@nestjs/common';
import { Resolver, Args } from '@nestjs/graphql';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { CommentModel } from '../comment.model';

@UseGuards(JwtAuthGuard)
@Resolver(() => CommentModel)
export class CommentQueryResolver {
  constructor(private readonly comment_service: CommentService) {}

  @Query(() => CommentModel)
  findOne(@Args('id') id: string) {
    return this.comment_service.find_one(id);
  }

  @Query(() => [CommentModel])
  findComments(@Args('data') pagination_data) {
    return this.comment_service.find_all(
      pagination_data.page_number,
      pagination_data.page_size,
    );
  }
}
