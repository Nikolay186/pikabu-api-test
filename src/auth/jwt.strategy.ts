import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UserDTO, UserEntity } from '../user/user.entity';
import { UnauthorizedException } from '@nestjs/common';
import { Repository } from 'typeorm';

export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly userRepo: Repository<UserEntity>) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET,
    });
  }

  async validate(payload: UserDTO) {
    const isUserExists = await this.userRepo.count({
      username: payload.username,
    });
    if (isUserExists === 0) {
      throw new UnauthorizedException();
    }
    return payload;
  }
}
