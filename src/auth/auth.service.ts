import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserEntity } from '../user/user.entity';
import { UserDTO } from '../user/user.entity';
import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private userRepo: Repository<UserEntity>,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(username: string, pwd: string): Promise<UserDTO> {
    const user: UserEntity = await this.userRepo.findOne(
      { username: username },
      { select: ['username', 'password', 'id', 'email'] },
    );
    if (user) {
      if ((await bcrypt.hash(pwd, 10)) !== user.password) {
        throw new UnauthorizedException();
      } else {
        delete user.password;
        return user;
      }
    }
  }

  signUser(id: string, username: string) {
    return this.jwtService.sign({
      id,
      username,
    });
  }
}
