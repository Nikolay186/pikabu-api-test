import { Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { CommentModel } from 'src/comment/comment.model';
import { BookmarkedCommentModel } from '../model/bookmarked.comment.model';
import { BookmarkFieldService } from '../service/bookmark.field.service';

@Resolver(() => BookmarkedCommentModel)
export class BookmarkedCommentFieldResolver {
  constructor(private readonly bookmark_service: BookmarkFieldService) {}

  @ResolveField('comment', () => CommentModel)
  find_one(@Parent() comment: BookmarkedCommentModel) {
    return this.bookmark_service.find_comment(comment.comment_id.toString());
  }
}
