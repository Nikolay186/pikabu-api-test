import { UseGuards } from '@nestjs/common';
import { Mutation, Resolver } from '@nestjs/graphql';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { Self } from 'src/user/self.decorator';
import { UserDTO } from 'src/user/user.entity';
import { BookmarkedCommentModel } from '../model/bookmarked.comment.model';
import { BookmarkedPostModel } from '../model/bookmarked.post.model';

@UseGuards(JwtAuthGuard)
@Resolver()
export class BookmarkResolver {
  constructor(private readonly bookmark_service: BookmarkService) {}

  @Mutation(() => BookmarkedCommentModel)
  bookmark_comment(@Self() user: UserDTO, comment_id: string) {
    return this.bookmark_service.bookmark_comment(user.id, comment_id);
  }

  @Mutation(() => BookmarkedCommentModel)
  unbookmark_comment(@Self() user: UserDTO, comment_id: string) {
    return this.bookmark_service.unbookmark_comment(user.id, comment_id);
  }

  @Mutation(() => BookmarkedPostModel)
  bookmark_post(@Self() user: UserDTO, post_id: string) {
    return this.bookmark_service.bookmark_post(user.id, post_id);
  }

  @Mutation(() => BookmarkedPostModel)
  unbookmark_post(@Self() user: UserDTO, post_id: string) {
    return this.bookmark_service.unbookmark_post(user.id, post_id);
  }
}
