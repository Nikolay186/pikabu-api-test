import { Resolver, ResolveField, Parent } from '@nestjs/graphql';
import { PostModel } from 'src/post/post.model';
import { BookmarkedPostModel } from '../model/bookmarked.post.model';
import { BookmarkFieldService } from '../service/bookmark.field.service';

@Resolver(() => BookmarkedPostModel)
export class BookmarkedPostFieldResolver {
  constructor(private readonly bookmark_service: BookmarkFieldService) {}

  @ResolveField('post', () => PostModel)
  find_one(@Parent() post: BookmarkedPostModel) {
    return this.bookmark_service.find_post(post.post_id.toString());
  }
}
