import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { BookmarkedCommentEntity } from '../entity/bookmarked.comment.entity';
import { BookmarkedPostEntity } from '../entity/bookmarked.post.entity';

@Injectable()
export class BookmarkService {
  constructor(
    private readonly bkmd_comment_repo: Repository<BookmarkedCommentEntity>,
    private readonly bkmd_post_repo: Repository<BookmarkedPostEntity>,
  ) {}

  async bookmark_comment(user_id: string, comment_id: string) {
    const comment = await this.bkmd_comment_repo.findOne({
      user_id: user_id,
      comment_id: comment_id,
    });
    if (!comment) {
      return await this.bkmd_comment_repo.save({
        user_id: user_id,
        comment_id: comment_id,
      });
    } else return comment;
  }

  async unbookmark_comment(user_id: string, comment_id: string) {
    const comment = await this.bkmd_comment_repo.findOne({
      user_id: user_id,
      comment_id: comment_id,
    });
    return this.bkmd_comment_repo.remove(comment);
  }

  async bookmark_post(user_id: string, post_id: string) {
    const post = await this.bkmd_post_repo.findOne({
      user_id: user_id,
      post_id: post_id,
    });
    if (!post) {
      return await this.bkmd_post_repo.save({
        user_id: user_id,
        post_id: post_id,
      });
    } else return post;
  }

  async unbookmark_post(user_id: string, post_id: string) {
    const post = await this.bkmd_post_repo.findOne({
      user_id: user_id,
      post_id: post_id,
    });
    return this.bkmd_post_repo.remove(post);
  }
}
