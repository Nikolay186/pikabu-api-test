import { CommentEntity } from 'src/comment/comment.entity';
import { PostEntity } from 'src/post/post.entity';
import { Repository } from 'typeorm';

export class BookmarkFieldService {
  constructor(
    private readonly comment_repo: Repository<CommentEntity>,
    private readonly post_repo: Repository<PostEntity>,
  ) {}

  async find_post(post_id: string) {
    return this.post_repo.findOne({ id: post_id });
  }

  async find_comment(comment_id: string) {
    return this.comment_repo.findOne({ id: comment_id });
  }
}
