import { CreateDateColumn } from 'typeorm';
import { Column } from 'typeorm';
import { PrimaryGeneratedColumn } from 'typeorm';

export class BookmarkedEntity {
  @PrimaryGeneratedColumn('increment')
  id: string;

  @Column()
  user_id: string;

  @CreateDateColumn()
  bookmarked_at: Date;
}
