import { PostEntity } from 'src/post/post.entity';
import { UserEntity } from 'src/user/user.entity';
import { Entity, Column, ManyToOne } from 'typeorm';
import { BookmarkedEntity } from './bookmarked.entity';

@Entity('Bookmarked_post')
export class BookmarkedPostEntity extends BookmarkedEntity {
  @Column()
  post_id: string;

  @ManyToOne(() => UserEntity, (user) => user.bookmarked_posts)
  user: UserEntity;

  @ManyToOne(() => PostEntity, (post) => post.bookmarks)
  post: PostEntity;
}
