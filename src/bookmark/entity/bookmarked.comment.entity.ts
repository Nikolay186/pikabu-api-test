import { CommentEntity } from 'src/comment/comment.entity';
import { UserEntity } from 'src/user/user.entity';
import { Column } from 'typeorm';
import { ManyToOne } from 'typeorm';
import { Entity } from 'typeorm';
import { BookmarkedEntity } from './bookmarked.entity';

@Entity('Bookmarked_comment')
export class BookmarkedCommentEntity extends BookmarkedEntity {
  @Column()
  comment_id: string;

  @ManyToOne(() => UserEntity, (user) => user.bookmarked_comments)
  user: UserEntity;

  @ManyToOne(() => CommentEntity, (comment) => comment.bookmarks)
  comment: CommentEntity;
}
