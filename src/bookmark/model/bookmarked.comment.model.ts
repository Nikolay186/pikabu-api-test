import { ObjectType, Field, Int } from '@nestjs/graphql';
import { CommentModel } from 'src/comment/comment.model';

@ObjectType()
export class BookmarkedCommentModel {
  @Field(() => Int)
  id: number;

  @Field(() => Int)
  comment_id: number;

  @Field(() => Date)
  bookmarked_at: Date;

  @Field(() => CommentModel)
  comment: CommentModel;
}
