import { ObjectType, Field, Int } from '@nestjs/graphql';
import { PostModel } from 'src/post/post.model';

@ObjectType()
export class BookmarkedPostModel {
  @Field(() => Int)
  id: number;

  @Field(() => Int)
  post_id: number;

  @Field(() => Date)
  bookmarked_at: Date;

  @Field(() => PostModel)
  post: PostModel;
}
