import { BookmarkedPostEntity } from 'src/bookmark/entity/bookmarked.post.entity';
import { CommentEntity } from 'src/comment/comment.entity';
import { ContentEntity } from 'src/content/content.entity';
import { PostReactionEntity } from 'src/reaction/entity/post.reaction.entity';
import { TagEntity } from 'src/tag/tag.entity';
import { UserEntity } from 'src/user/user.entity';
import { Column } from 'typeorm';
import { OneToMany } from 'typeorm';
import { JoinTable } from 'typeorm';
import { ManyToMany } from 'typeorm';
import { ManyToOne } from 'typeorm';
import { CreateDateColumn } from 'typeorm';
import { PrimaryGeneratedColumn } from 'typeorm';
import { Entity } from 'typeorm';

@Entity('Posts')
export class PostEntity {
  @PrimaryGeneratedColumn('increment')
  id: string;

  @Column('text')
  title: string;

  @Column('text')
  content: string;

  @Column()
  owner_id: string;

  @CreateDateColumn()
  created_date: Date;

  @ManyToOne(() => UserEntity, (user) => user.posts)
  owner: UserEntity;

  @OneToMany(() => CommentEntity, (comment) => comment.post)
  comments?: CommentEntity[];

  @ManyToMany(() => TagEntity, (tag) => tag.post)
  @JoinTable({ name: 'post_tags' })
  tags: TagEntity[];

  @OneToMany(() => ContentEntity, (attachment) => attachment.post)
  attachments?: ContentEntity[];

  @OneToMany(() => PostReactionEntity, (reaction) => reaction.post)
  reactions?: PostReactionEntity[];

  @OneToMany(
    () => BookmarkedPostEntity,
    (bookmarked_post) => bookmarked_post.post,
  )
  bookmarks?: BookmarkedPostEntity[];
}
