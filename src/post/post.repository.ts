import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm';
import { PostEntity } from './post.entity';

@EntityRepository(PostEntity)
export class PostRepository extends Repository<PostEntity> {
  async get_fresh(
    page: number,
    size: number,
    tags: string[],
    sort_mode: PostOrdering,
  ) {
    const yesterday = new Date(Date.now() - 864e5);
    const query = this.createQueryBuilder('post')
      .skip(page * size)
      .limit(size)
      .where('"created_date" > :yesterday', { yesterday: yesterday });

    if (sort_mode == PostOrdering.Date) {
      query.orderBy('created_date');
    } else if (sort_mode == PostOrdering.Likes) {
      query.orderBy('rating');
    }
    if (tags) {
      query
        .leftJoin('post.tags', 'tag')
        .where('tag.name IN (:...tags)', { tags: tags });
    }
    return await query.getRawMany();
  }

  async get_hot(
    page: number,
    size: number,
    tags: string[],
    sort_mode: PostOrdering,
  ) {
    const yesterday = new Date(Date.now() - 864e5);
    const query = this.createQueryBuilder('post')
      .skip(page * size)
      .limit(size)
      .where('"created_date" > :yesterday', { yesterday: yesterday });

    if (sort_mode == PostOrdering.Date) {
      query.orderBy('created_date');
    } else if (sort_mode == PostOrdering.Likes) {
      query.orderBy('rating');
    }
    if (tags) {
      query
        .leftJoin('post.tags', 'tag')
        .where('tag.name IN (:...tags)', { tags: tags });
    }

    query.orderBy('comment_amount');
    return await query.getRawMany();
  }

  async get_best(page: number, size: number, tags: string[]) {
    const yesterday = new Date(Date.now() - 864e5);
    const query = this.createQueryBuilder('post')
      .skip(page * size)
      .limit(size)
      .where('"created_date" > :yesterday', { yesterday: yesterday });

    if (tags) {
      query
        .leftJoin('post.tags', 'tag')
        .where('tag.name IN (:...tags)', { tags: tags });
    }

    query.orderBy('rating');
    return await query.getRawMany();
  }

  async get_by_title(title: string) {
    const query = this.createQueryBuilder('post').where('post.title = :title', {
      title: title,
    });
    return await query.getOne();
  }
}

export enum PostOrdering {
  'Date' = 0,
  'Likes' = 1,
}
