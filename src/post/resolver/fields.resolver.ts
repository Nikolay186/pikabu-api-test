import { Resolver, ResolveField, Parent, Int } from '@nestjs/graphql';
import { UserModel } from 'src/user/model/user.model';
import { PostModel } from '../post.model';

@Resolver(() => PostModel)
export class PostFieldResolver {
  constructor(private readonly postFieldService: PostFieldService) {}

  @ResolveField('author', () => UserModel)
  author(@Parent() post: PostModel) {
    return this.postFieldService.author(post.id);
  }

  @ResolveField('tags', () => [String])
  tags(@Parent() post: PostModel) {
    return this.postFieldService.tags(post.id);
  }

  @ResolveField('rating', () => Int)
  async rating(@Parent() post: PostModel) {
    return this.postFieldService.rating(post.id);
  }

  @ResolveField('userReactions', () => [UserReactionModel])
  async userReactions(@Parent() post: PostModel) {
    return this.postFieldService.userReactions(post.id);
  }

  @ResolveField('comments', () => [CommentModel])
  async comments(@Parent() post: PostModel) {
    return this.postFieldService.comments(post.id);
  }

  @ResolveField('commentAmount', () => Int)
  async commentAmount(@Parent() post: PostModel) {
    return (await this.postFieldService.comments(post.id)).length;
  }

  @ResolveField('contents', () => [PostContentModel])
  async contents(@Parent() post: PostModel) {
    return this.postFieldService.contents(post.id);
  }
}
