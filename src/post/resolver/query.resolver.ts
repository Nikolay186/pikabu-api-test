import { Args, Query, Resolver } from '@nestjs/graphql';
import { PostModel } from '../post.model';

@Resolver(() => PostModel)
export class PostQueryResolver {
  constructor(private readonly postService: PostService) {}

  @Query(() => [PostModel], { name: 'posts' })
  async find_all(@Args('data') pagination_data) {
    return this.postService.find_all(
      pagination_data.page_number,
      pagination_data.page_size,
    );
  }

  @Query(() => PostModel, { name: 'post' })
  getOnePost(@Args('id') id: number) {
    return this.postService.find_one(id);
  }
}
