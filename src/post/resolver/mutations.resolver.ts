import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { Self } from 'src/user/self.decorator';
import { UserDTO } from 'src/user/user.entity';
import { CreatePostInput } from '../input/create.input';
import { UpdatePostInput } from '../input/update.input';
import { PostModel } from '../post.model';

@Resolver(() => PostModel)
export class PostMutationResolver {
  constructor(private readonly postService: PostService) {}

  @Mutation(() => PostModel)
  async create_post(
    @Self() user: UserDTO,
    @Args('data') data: CreatePostInput,
  ) {
    return this.postService.create(user.id, data);
  }

  @Mutation(() => PostModel)
  async update_post(
    @Self() user: UserDTO,
    @Args('data') data: UpdatePostInput,
  ) {
    return this.postService.update(user.id, data);
  }

  @Mutation(() => PostModel)
  async delete_post(@Self() user: UserDTO, @Args('id') post_id: number) {
    return this.postService.delete(user.id, post_id);
  }
}
