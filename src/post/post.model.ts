import { ObjectType, Field, Int } from '@nestjs/graphql';
import { CommentModel } from 'src/comment/comment.model';
import { ContentModel } from 'src/content/content.model';
import { UserReactionModel } from 'src/reaction/model/user.reaction.model';
import { TagModel } from 'src/tag/tag.model';
import { UserModel } from 'src/user/model/user.model';

@ObjectType()
export class PostModel {
  @Field(() => Int)
  id: number;

  @Field(() => String)
  title: string;

  @Field(() => String)
  text: string;

  @Field(() => Date)
  created_date: Date;

  @Field(() => Date)
  updated_date: Date;

  @Field(() => UserModel)
  author?: UserModel;

  @Field(() => Int)
  author_id: number;

  @Field(() => [UserReactionModel])
  user_reactions?: UserReactionModel[];

  @Field(() => [CommentModel])
  comments?: CommentModel[];

  @Field(() => [String])
  tags: TagModel[];

  @Field(() => Int)
  rating?: number;

  @Field(() => Int)
  comment_amount?: number;

  @Field(() => [ContentModel])
  attachments?: ContentModel[];
}
