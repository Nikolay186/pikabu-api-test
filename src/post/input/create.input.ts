import { Field, InputType } from '@nestjs/graphql';
import { IsString, MinLength, IsArray, IsNotEmpty } from 'class-validator';

@InputType()
export class CreatePostInput {
  @IsString()
  @MinLength(3)
  @Field(() => String, { nullable: false })
  title: string;

  @IsString()
  @MinLength(3)
  @Field(() => String, { nullable: false })
  text: string;

  @IsArray()
  @IsString({ each: true })
  @IsNotEmpty({ each: true })
  @Field(() => [String], { nullable: false })
  tags: string[];

  @IsArray()
  @IsString({ each: true })
  @IsNotEmpty({ each: true })
  @Field(() => [String], { nullable: true })
  attachments_urls?: string[];
}
