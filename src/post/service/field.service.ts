import { Injectable } from '@nestjs/common';

@Injectable()
export class PostsFieldsService {
  constructor(
    private readonly postRepository: PostRepository,
    private readonly commentRepository: CommentRepository,
    private readonly postReactionRepository: PostReactionRepository,
    private readonly postContentRepository: PostContentRepository,
  ) {}

  async author(id: string) {
    return (await this.postRepository.findOne(id, { relations: ['author'] }))
      .author;
  }

  async comments(postId: string) {
    return this.commentRepository.find({ postId });
  }

  async userReactions(postId: string) {
    return this.postReactionRepository.find({ postId });
  }

  async tags(id: string) {
    return (
      await this.postRepository.findOne(id, { relations: ['tags'] })
    ).tags.map((tag) => tag.name);
  }

  async rating(id: string) {
    return this.postRepository.getPostRating(id);
  }

  async contents(postId: string) {
    return this.postContentRepository.find({ postId });
  }
}
