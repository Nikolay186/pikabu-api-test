import { Injectable } from '@nestjs/common';
import { TagEntity } from 'src/tag/tag.entity';
import { Repository } from 'typeorm';
import { CreatePostInput } from '../input/create.input';
import { UpdatePostInput } from '../input/update.input';
import { PostModel } from '../post.model';
import { PostOrdering, PostRepository } from '../post.repository';

@Injectable()
export class PostService {
  constructor(
    private readonly post_repo: PostRepository,
    private readonly tag_repo: Repository<TagEntity>,
  ) {}

  async create(owner_id: string, create_post_input: CreatePostInput) {
    const tags = await this.tag_repo.save(create_post_input.tags);
    const attachments = create_post_input.attachments_urls?.map((url) => {
      return { url: url };
    });
    return await this.post_repo.save({
      ...create_post_input,
      owner_id: owner_id,
      tags: tags,
      attachments: attachments,
    });
  }

  async find_one(post_id: string) {
    return this.post_repo.findOne(post_id);
  }

  async find_all(page: number, size: number) {
    return this.post_repo.find({ take: size, skip: size * page });
  }

  async update(
    user_id: string,
    post_id: string,
    update_post_input: UpdatePostInput,
  ) {
    const post = await this.post_repo.findOne(post_id);
    if (post.owner_id === user_id) {
      const tags = await this.tag_repo.save(update_post_input.tags);
      const attachments = update_post_input.contentUrls?.map((url) => {
        return { url: url };
      });
      return this.post_repo.save({
        ...post,
        ...update_post_input,
        tags: tags,
        attachments: attachments,
      });
    }
  }

  async remove(user_id: string, post_id: string) {
    const post = await this.post_repo.findOne(post_id);
    if (user_id == post.owner_id) {
      return this.post_repo.remove(post);
    }
  }

  async find(
    page: number,
    size: number,
    category: Category,
    tags: string[],
    title: string,
    ordering: PostOrdering,
  ) {
    let posts: PostModel[] = [];
    let post_count;
    switch (category) {
      case Category.Best:
        posts = await this.post_repo.get_best(page, size, tags);
        post_count = posts.length;
        break;
      case Category.Fresh:
        posts = await this.post_repo.get_fresh(page, size, tags, ordering);
        post_count = posts.length;
        break;
      case Category.Hot:
        posts = await this.post_repo.get_hot(page, size, tags, ordering);
        post_count = posts.length;
        break;
    }

    return { count: post_count, posts: posts };
  }

  async find_by_title(title: string) {
    if (title != '') {
      return await this.post_repo.get_by_title(title);
    }
  }
}

enum Category {
  'Fresh' = 0,
  'Hot' = 1,
  'Best' = 2,
}
