import { ObjectType, Field, Int } from '@nestjs/graphql';
import { PostModel } from 'src/post/post.model';
import { Reaction } from '../reaction.enum';

@ObjectType()
export class PostReactionModel {
  @Field(() => Int)
  id: number;

  @Field(() => Int)
  post_id: number;

  @Field(() => PostModel)
  post: PostModel;

  @Field(() => Reaction)
  reaction: number;
}
