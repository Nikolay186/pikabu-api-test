import { ObjectType, Field, Int } from '@nestjs/graphql';
import { CommentModel } from 'src/comment/comment.model';
import { Reaction } from '../reaction.enum';

@ObjectType()
export class CommentReactionModel {
  @Field(() => Int)
  id: number;

  @Field(() => Int)
  comment_id: number;

  @Field(() => CommentModel)
  comment: CommentModel;

  @Field(() => Reaction)
  reaction: number;
}
