import { ObjectType, Field, Int } from '@nestjs/graphql';
import { UserModel } from 'src/user/model/user.model';
import { Reaction } from '../reaction.enum';

@ObjectType()
export class UserReactionModel {
  @Field(() => Int)
  id: number;

  @Field(() => Int)
  user_id: number;

  @Field(() => UserModel)
  user: UserModel;

  @Field(() => Reaction)
  reaction: number;
}
