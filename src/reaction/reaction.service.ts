import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { CommentReactionEntity } from './entity/comment.reaction.entity';
import { PostReactionEntity } from './entity/post.reaction.entity';
import { Reaction } from './reaction.enum';

@Injectable()
export class ReactionService {
  constructor(
    private readonly post_react_repo: Repository<PostReactionEntity>,
    private readonly comm_react_repo: Repository<CommentReactionEntity>,
  ) {}

  async react_post(user_id: string, post_id: string, reaction: number) {
    const current_reaction = await this.post_react_repo.findOne({
      user_id: user_id,
      post_id: post_id,
    });
    if (current_reaction) {
      await this.post_react_repo.update(current_reaction, { reaction });
      current_reaction.reaction = reaction;
      return current_reaction;
    }
    return await this.post_react_repo.save({ user_id, post_id, reaction });
  }

  async like_post(user_id: string, post_id: string) {
    return this.react_post(user_id, post_id, Reaction.Like);
  }

  async dislike_post(user_id: string, post_id: string) {
    return this.react_post(user_id, post_id, Reaction.Dislike);
  }

  async react_comment(user_id: string, comment_id: string, reaction: number) {
    const current_reaction = await this.comm_react_repo.findOne({
      user_id: user_id,
      comment_id: comment_id,
    });
    if (current_reaction) {
      await this.comm_react_repo.update(current_reaction, { reaction });
      current_reaction.reaction = reaction;
      return current_reaction;
    }
    return await this.comm_react_repo.save({ user_id, comment_id, reaction });
  }

  async like_comment(user_id: string, comment_id: string) {
    return this.react_comment(user_id, comment_id, Reaction.Like);
  }

  async dislike_comment(user_id: string, comment_id: string) {
    return this.react_comment(user_id, comment_id, Reaction.Dislike);
  }
}
