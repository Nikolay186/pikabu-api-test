import { Resolver, ResolveField, Parent } from '@nestjs/graphql';
import { PostModel } from 'src/post/post.model';
import { PostReactionModel } from '../model/post.reaction.model';

@Resolver(PostReactionModel)
export class PostReactionFieldResolver {
  constructor(private readonly field_service: ReactionFieldService) {}

  @ResolveField('post', () => PostModel)
  async find_one(@Parent() reaction: PostReactionModel) {
    return this.field_service.find_post(reaction.post_id);
  }
}
