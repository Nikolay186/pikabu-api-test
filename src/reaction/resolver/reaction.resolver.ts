import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { Self } from 'src/user/self.decorator';
import { UserDTO } from 'src/user/user.entity';
import { CommentReactionModel } from '../model/comment.reaction.model';
import { PostReactionModel } from '../model/post.reaction.model';
import { ReactionService } from '../reaction.service';

@UseGuards(JwtAuthGuard)
@Resolver()
export class ReactionResolver {
  constructor(private readonly reaction_service: ReactionService) {}

  @Mutation(() => PostReactionModel)
  like_post(@Self() user: UserDTO, @Args('id') post_id: string) {
    return this.reaction_service.like_post(user.id, post_id);
  }

  @Mutation(() => PostReactionModel)
  dislike_post(@Self() user: UserDTO, @Args('id') post_id: string) {
    return this.reaction_service.dislike_post(user.id, post_id);
  }

  @Mutation(() => CommentReactionModel)
  like_comment(@Self() user: UserDTO, @Args('id') comment_id: string) {
    return this.reaction_service.like_comment(user.id, comment_id);
  }

  @Mutation(() => CommentReactionModel)
  dislike_comment(@Self() user: UserDTO, @Args('id') comment_id: string) {
    return this.reaction_service.dislike_comment(user.id, comment_id);
  }
}
