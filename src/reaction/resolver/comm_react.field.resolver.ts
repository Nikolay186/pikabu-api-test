import { Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { CommentModel } from 'src/comment/comment.model';
import { CommentReactionModel } from '../model/comment.reaction.model';

@Resolver(CommentReactionModel)
export class CommentReactionFieldResolver {
  constructor(private readonly field_service: ReactionFieldService) {}

  @ResolveField('comment', () => CommentModel)
  async find_one(@Parent() reaction: CommentReactionModel) {
    return this.field_service.find_comment(reaction.comment_id);
  }
}
