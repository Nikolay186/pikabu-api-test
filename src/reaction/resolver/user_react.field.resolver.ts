import { Resolver, ResolveField, Parent } from '@nestjs/graphql';
import { UserModel } from 'src/user/model/user.model';
import { UserReactionModel } from '../model/user.reaction.model';

@Resolver(UserReactionModel)
export class UserReactionFieldResolver {
  constructor(private readonly field_service: ReactionFieldService) {}

  @ResolveField('post', () => UserModel)
  async find_one(@Parent() reaction: UserReactionModel) {
    return this.field_service.find_user(reaction.user_id);
  }
}
