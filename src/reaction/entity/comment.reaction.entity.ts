import { CommentEntity } from 'src/comment/comment.entity';
import { UserEntity } from 'src/user/user.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';

@Entity('Comment_reaction')
export class CommentReactionEntity {
  @PrimaryGeneratedColumn('increment')
  id: string;

  @Column()
  comment_id: string;

  @Column()
  user_id: string;

  @Column()
  reaction: number;

  @ManyToOne(() => CommentEntity, (comment) => comment.user_reactions)
  comment?: CommentEntity;

  @ManyToOne(() => UserEntity, (user) => user.comments_reactions)
  user?: UserEntity;
}
