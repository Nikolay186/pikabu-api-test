import { CommentEntity } from 'src/comment/comment.entity';
import { PostEntity } from 'src/post/post.entity';
import { UserEntity } from 'src/user/user.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';

@Entity('Post_reaction')
export class PostReactionEntity {
  @PrimaryGeneratedColumn('increment')
  id: string;

  @Column()
  post_id: string;

  @Column()
  user_id: string;

  @Column()
  reaction: number;

  @ManyToOne(() => PostEntity, (post) => post.reactions)
  post?: CommentEntity;

  @ManyToOne(() => UserEntity, (user) => user.posts_reactions)
  user?: UserEntity;
}
