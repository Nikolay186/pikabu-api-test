import { Field, InputType } from '@nestjs/graphql';
import { IsEmail, IsNotEmpty } from 'class-validator';

@InputType()
export class UpdateUserInput {
  @IsEmail()
  @IsNotEmpty()
  @Field(() => String)
  email: string;
}
