import { Injectable } from '@nestjs/common';
import { AuthService } from 'src/auth/auth.service';

@Injectable()
export class UserAuthService {
  constructor(private readonly authService: AuthService) {}

  async login(username: string, pwd: string) {
    const user_dto = await this.authService.validateUser(username, pwd);
    const token = this.authService.signUser(user_dto.id, user_dto.email);

    return { ...user_dto, token: token };
  }
}
