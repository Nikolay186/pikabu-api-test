import { Injectable } from '@nestjs/common';
import { CommentEntity } from 'src/comment/comment.entity';
import { PostEntity } from 'src/post/post.entity';
import { CommentReactionEntity } from 'src/reaction/entity/comment.reaction.entity';
import { PostReactionEntity } from 'src/reaction/entity/post.reaction.entity';
import { Repository } from 'typeorm';
import { UserEntity } from '../user.entity';

@Injectable()
export class UserFieldService {
  constructor(
    private readonly userRepo: Repository<UserEntity>,
    private readonly postRepo: Repository<PostEntity>,
    private readonly postReactionRepo: Repository<PostReactionEntity>,
    private readonly commentRepo: Repository<CommentEntity>,
    private readonly commentReactionRepo: Repository<CommentReactionEntity>,
  ) {}

  async posts(owner_id: string) {
    return this.postRepo.find({
      owner_id: owner_id,
    });
  }

  async bookmarked_posts(id: string, page: number, size: number) {
    const posts = await this.userRepo.findOne(
      { id: id },
      { relations: ['bookmarked_posts'] },
    );
    const posts_id = posts.bookmarked_posts.map((post) => post.id);
    return this.postRepo.findByIds(posts_id, { skip: page * size, take: size });
  }

  async post_reactions(owner_id: string) {
    return this.postReactionRepo.find({ owner_id: owner_id });
  }

  async comments(owner_id: string) {
    return this.commentRepo.find({
      owner_id: owner_id,
    });
  }

  async bookmarked_comments(id: string, page: number, size: number) {
    const comments = await this.userRepo.findOne(
      { id: id },
      { relations: ['bookmarked_comments'] },
    );
    const comments_id = comments.bookmarked_comments.map(
      (comment) => comment.id,
    );
    return this.commentRepo.findByIds(comments_id, {
      skip: page * size,
      take: size,
    });
  }

  async comment_reactions(owner_id: string) {
    return this.commentReactionRepo.find({ owner_id: owner_id });
  }
}
