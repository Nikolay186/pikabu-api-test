import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class UserModel {
  // common info
  @Field(() => Int)
  id: number;

  @Field(() => String)
  username: string;

  @Field(() => String)
  email: string;

  @Field(() => Date)
  registration_date: Date;

  // posts
  @Field(() => [PostModel])
  posts?: PostModel[];

  @Field(() => [PostModel])
  bookmarked_posts?: PostModel[];

  @Field(() => [PostReactionModel])
  posts_reactions?: PostReactionModel[];

  // comments
  @Field(() => [CommentModel])
  comments?: CommentModel[];

  @Field(() => [CommentModel])
  bookmarked_comments?: CommentModel[];

  @Field(() => [CommentReactionModel])
  comments_reactions?: CommentReactionModel[];
}
