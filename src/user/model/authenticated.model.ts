import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class AuthenticatedUserModel {
  @Field(() => Int)
  id: number;

  @Field(() => String)
  username: string;

  @Field(() => String)
  token: string;
}
