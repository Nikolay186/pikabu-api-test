import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('User')
export class UserEntity {
  @PrimaryGeneratedColumn('increment')
  id: string;

  // common info
  @Column('text')
  username: string;

  @Column('text')
  email: string;

  @Column('text')
  password: string;

  @CreateDateColumn()
  registration_date: Date;

  // posts
  @OneToMany(() => PostEntity, (post) => post.owner)
  posts?: PostEntity[];

  @OneToMany(() => PostReactionEntity, (reaction) => reaction.owner)
  posts_reactions?: PostReactionEntity[];

  @OneToMany(
    () => BookmarkedPostEntity,
    (bookmarked_post) => bookmarked_post.owner,
  )
  bookmarked_posts?: BookmarkedPostEntity[];

  // comments
  @OneToMany(() => CommentEntity, (comment) => comment.owner)
  comments?: CommentEntity[];

  @OneToMany(() => CommentReactionEntity, (reaction) => reaction.owner)
  comments_reactions?: CommentReactionEntity[];

  @OneToMany(
    () => BookmarkedCommentEntity,
    (bookmarked_comment) => bookmarked_comment.owner,
  )
  bookmarked_comments?: BookmarkedCommentEntity[];
}

export class UserDTO {
  id: string;
  username: string;
  email: string;
}
