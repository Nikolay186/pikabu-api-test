import { Args, Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { UserModel } from '../model/user.model';

@Resolver(() => UserModel)
export class UserFieldResolver {
  constructor(private readonly userService: UserFieldService) {}

  // posts
  @ResolveField('posts', () => [PostModel])
  async posts(@Parent() user: UserModel, @Args('data') pagination_data) {
    return this.userService.posts(
      user.id,
      pagination_data.page_number,
      pagination_data.page_size,
    );
  }

  @ResolveField('bookmarked_posts', () => [PostModel])
  async bookmarked_posts(
    @Parent() user: UserModel,
    @Args('data') pagination_data,
  ) {
    return this.userService.bookmarked_posts(
      user.id,
      pagination_data.page_number,
      pagination_data.page_size,
    );
  }

  @ResolveField('posts_reactions', () => [PostReactionModel])
  async posts_reactions(
    @Parent() user: UserModel,
    @Args('data') pagination_data,
  ) {
    return this.userService.posts_reactions(
      user.id,
      pagination_data.page_number,
      pagination_data.page_size,
    );
  }

  // comments
  @ResolveField('comments', () => [CommentModel])
  async comments(@Parent() user: UserModel, @Args('data') pagination_data) {
    return this.userService.comments(
      user.id,
      pagination_data.page_number,
      pagination_data.page_size,
    );
  }

  @ResolveField('bookmarked_comments', () => [CommentModel])
  async bookmarked_comments(
    @Parent() user: UserModel,
    @Args('data') pagination_data,
  ) {
    return this.userService.bookmarked_comments(
      user.id,
      pagination_data.page_number,
      pagination_data.page_size,
    );
  }

  @ResolveField('comments_reactions', () => [CommentReactionModel])
  async comments_reactions(
    @Parent() user: UserModel,
    @Args('data') pagination_data,
  ) {
    return this.userService.comments_reactions(
      user.id,
      pagination_data.page_number,
      pagination_data.page_size,
    );
  }
}
