import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { UpdateUserInput } from '../input/update.input';
import { UserModel } from '../model/user.model';
import { Self } from '../self.decorator';
import { UserDTO } from '../user.entity';

@Resolver(() => UserModel)
export class UserMutationResolver {
  constructor(private readonly userService: UserService) {}

  @Mutation(() => UserModel)
  update_user(@Self() user: UserDTO, @Args('data') data: UpdateUserInput) {
    return this.userService.update(user.id, data);
  }
}
