import { Query } from '@nestjs/common';
import { Args, Resolver } from '@nestjs/graphql';
import { UserModel } from '../model/user.model';
import { Self } from '../self.decorator';
import { UserDTO } from '../user.entity';

@Resolver(() => UserModel)
export class UserQueryResolver {
  constructor(private readonly userService: UserService) {}

  @Query(() => UserModel)
  async find_one(@Args('id') id: number) {
    return this.userService.find_one(id);
  }

  @Query(() => UserModel)
  async whoami(@Self() self: UserDTO) {
    return this.userService.find_one(self.id);
  }

  @Query(() => [UserModel])
  async find_all(@Args('data') pagination_data) {
    return this.userService.find_all(
      pagination_data.page_number,
      pagination_data.page_size,
    );
  }
}
