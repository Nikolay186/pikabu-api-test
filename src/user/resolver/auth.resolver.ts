import { Query } from '@nestjs/common';
import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { LoginInput } from '../input/login.input';
import { RegisterInput } from '../input/register.input';
import { AuthenticatedUserModel } from '../model/authenticated.model';
import { UserModel } from '../model/user.model';

@Resolver(() => UserModel)
export class UserAuthResolver {
  constructor(private readonly userService: UserService) {}

  @Mutation(() => UserModel)
  register(@Args('input') data: RegisterInput) {
    return this.userService.register(data);
  }

  @Query(() => AuthenticatedUserModel)
  login(@Args('input') data: LoginInput) {
    return this.userService.login(data);
  }
}
